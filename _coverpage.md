![logo](tongshanglogo_small.png)

# 《速易天工V3 生产进度管理软件》

!> 专业的企业生产管理软件

* 生产进度看板
* 扫码报工
* 自动生成多种生产报表
* 集成app，对接erp

[官网](https://www.6erp.cn/portfoliotype/messp)
[快速浏览](latest/guide)